<?php

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('tablepaciente', 'PacienteController@show')->name('tablepaciente');
Route::get('tablemedico', 'MedicoController@show')->name('tablemedico');
Route::get('tablepersonalsalud', 'PersonalSaludController@show')->name('tablepersonalsalud');
Route::get('tablecitas', 'CitaController@show')->name('tablecitas');
Route::get('tablemedicinas', 'MedicinaController@show')->name('tablemedicinas');
Route::get('tableprocedimiento', 'ProcedimientoController@show')->name('tableprocedimiento');
Route::get('tableCitasMedico', 'CitaController@showCitaMedico')->name('tableCitasMedico');


Route::get('tableespecialidad', 'EspecialidadController@show')->name('tableespecialidad');
Route::get('tablehistorialprocedimientos', 'HistorialProcedimientoController@show')->name('tablehistorialprocedimientos');


Route::get('/edit/{cedula}', 'PacienteController@preedit')->name('/edit/{cedula}');
Route::post('/mod/{cedula}', 'PacienteController@edit');


// Authentication Routes...
Route::get('selectlogin', 'Auth\LoginController@show')->name('selectlogin');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('loginmedico', 'MedicoController@showlogin')->name('loginmedico');
Route::post('loginmedico', 'MedicoController@openHome');
Route::get('loginpaciente', 'PacienteController@showlogin')->name('loginpaciente');
Route::post('loginpaciente', 'PacienteController@openHome');
Route::get('MedicoRegistro', 'PersonaController@registro')->name('registroMedico');
Route::post('login', 'Auth\LoginController@login');
Route::post('/logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

// Registration Routes...
if ($options['register'] ?? true) {
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
}
if ($options['registeradministrador'] ?? true) {
    Route::get('registeradministrador', 'AdministradorController@registro')->name('registeradministrador');
}
if ($options['registerpersona'] ?? true) {
	Route::get('registerpersona', 'PersonaController@registro')->name('registerpersona');
   	Route::post('registerpersona', 'PersonaController@register');
}
if ($options['registermedico'] ?? true) {
	Route::get('registermedico', 'MedicoController@registro')->name('registermedico');
    Route::post('registermedico', 'MedicoController@register');
}
if ($options['registerpaciente'] ?? true) {
	Route::get('registerpaciente', 'PacienteController@registro')->name('registerpaciente');
    Route::post('registerpaciente', 'PacienteController@register');
}
if ($options['registerespecialidad'] ?? true) {
	Route::get('registerespecialidad', 'EspecialidadController@registro')->name('registerespecialidad');
    Route::post('registerespecialidad', 'EspecialidadController@register');
}
if ($options['registerhistorialespecialidad'] ?? true) {
	Route::get('registerhistorialespecialidad', 'HistorialEspecialidadController@registro')->name('registerhistorialespecialidad');
    Route::post('registerhistorialespecialidad', 'HistorialEspecialidadController@register');
}
if ($options['registerpersonalsalud'] ?? true) {
    Route::get('registerpersonalsalud', 'PersonalSaludController@registro')->name('registerpersonalsalud');
    Route::post('registerpersonalsalud', 'PersonalSaludController@register');
}
if ($options['registerdiagnostico'] ?? true) {
    Route::get('registerdiagnostico', 'DiagnosticoController@registro')->name('registerdiagnostico');
    Route::post('registerdiagnostico', 'DiagnosticoController@register');
}
if ($options['registerhistorialMedicina'] ?? true) {
    Route::get('registerhistorialMedicina', 'HistorialMedicinaController@registro')->name('registerhistorialMedicina');
    Route::post('registerhistorialMedicina', 'HistorialMedicinaController@register');
}
if ($options['registertratamiento'] ?? true) {
    Route::get('registertratamiento', 'TratamientoController@registro')->name('registertratamiento');
    Route::post('registertratamiento', 'TratamientoController@register');
}
if ($options['registerprocedimiento'] ?? true) {
    Route::get('registerprocedimiento', 'ProcedimientoController@registro')->name('registerprocedimiento');
    Route::post('registerprocedimiento', 'ProcedimientoController@register');
}
if ($options['registerMedicina'] ?? true) {
    Route::get('registerMedicina', 'MedicinaController@registro')->name('registerMedicina');
    Route::post('registerMedicina', 'MedicinaController@register');
}
if ($options['registerhistorialProcedimiento'] ?? true) {
    Route::get('registerhistorialProcedimiento', 'HistorialProcedimientoController@registro')->name('registerhistorialProcedimiento');
    Route::post('registerhistorialProcedimiento', 'HistorialProcedimientoController@register');
}
if ($options['registerhistorialPersonalSalud'] ?? true) {
    Route::get('registerhistorialPersonalSalud', 'HistorialPersonalSaludController@registro')->name('registerhistorialPersonalSalud');
    Route::post('registerhistorialPersonalSalud', 'HistorialPersonalSaludController@register');
}

if ($options['registercitas'] ?? true) {
    Route::get('registercitas', 'CitaController@registro')->name('registercitas');
    Route::post('registercitas', 'CitaController@register');
}
// Password Reset Routes...
if ($options['reset'] ?? true) {
    Route::resetPassword();
}

// Email Verification Routes...
if ($options['verify'] ?? false) {
    Route::emailVerification();
}

//Reset Password

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


//Verificar Email

Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');



Route::get('/home', 'HomeController@index')->name('home');

