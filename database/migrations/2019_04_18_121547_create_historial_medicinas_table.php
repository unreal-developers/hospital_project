<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorialMedicinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historial_medicinas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('fecha_prescripcion');
            $table->date('fecha_final_consumo');
            $table->text('descripcion');           
            $table->bigInteger('id_tratamiento')->unsigned();
            $table->foreign('id_tratamiento')->references('id')->on('tratamientos');
            $table->bigInteger('id_medicina')->unsigned();
            $table->foreign('id_medicina')->references('id')->on('medicinas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historial_medicinas');
    }
}
