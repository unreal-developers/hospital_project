<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalSalud extends Model
{
	protected $fillable = [
        'cargo','area_Trabajo','fecha_ingreso_hospital','estado_contrato', 'id_persona',
    ];
    public function persona(){
    	return $this->belongsTo('App\Persona');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function HistorialPersonalSalud(){
    	return $this->hasMany('App\HistorialPersonalSalud');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
