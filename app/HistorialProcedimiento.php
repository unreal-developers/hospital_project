<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialProcedimiento extends Model
{
    protected $fillable = [
        'fecha_realizacion','estado_realizacion','id_tratamiento','id_procedimiento',
    ];
    
    public function Tratamiento(){
    	return $this->belongsTo('App\Tratamiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Procedimiento(){
    	return $this->belongsTo('App\Procedimiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function HistorialPersonalSalud(){
    	return $this->hasMany('App\HistorialPersonalSalud');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    
}
