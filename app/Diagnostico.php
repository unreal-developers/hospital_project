<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model
{
    protected $fillable = [
        'nombre_clave','descripcion','id_cita',
    ];
    
    public function Cita(){
    	return $this->belongsTo('App\Cita');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Tratamiento(){
    	return $this->belongsTo('App\Tratamiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
