<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{

    protected $fillable = [
        'name','apellido', 'cedula','fecha_nacimiento','edad','sexo','tipo_Sangre', 'telefono_Personal','telefono_Emergencia', 'email','direccion','password',
    ];


    public function Paciente(){
    	return $this->belongsTo('App\Paciente');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function PersonalSalud(){
    	return $this->belongsTo('App\PersonalSalud');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Medico(){
    	return $this->belongsTo('App\Medico');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
