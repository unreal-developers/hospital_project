<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $fillable = [
        'nombre','descripcion',
    ];
    public function historialespecialidad(){
    	return $this->hasMany('App\HistorialEspecialidad');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
