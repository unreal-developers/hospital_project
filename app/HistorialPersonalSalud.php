<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialPersonalSalud extends Model
{
    protected $fillable = [
        'descripcion','id_personal_saluds','id_historial_procedimientos',
    ];
    
    
    public function HistorialProcedimiento(){
    	return $this->belongsTo('App\HistorialProcedimiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function PersonalSalud(){
    	return $this->belongsTo('App\PersonalSalud');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
