<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialMedicina extends Model
{
    protected $fillable = [
        'fecha_prescripcion','fecha_final_consumo','descripcion','id_tratamiento','id_medicina',
    ];
    
    public function Tratamiento(){
    	return $this->belongsTo('App\Tratamiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function Medicinas(){
    	return $this->belongsTo('App\Medicinas');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
