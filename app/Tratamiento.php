<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tratamiento extends Model
{
    protected $fillable = [
        'descripcion_Medicamentos','descripcion_Operaciones','id_diagnostico',
    ];
    
    public function Diagnostico(){
    	return $this->belongsTo('App\Diagnostico');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function HistorialMedicina(){
    	return $this->hasMany('App\HistorialMedicina');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function HistorialProcedimiento(){
    	return $this->hasMany('App\HistorialProcedimiento');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
}
