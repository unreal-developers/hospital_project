<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
	protected $fillable = [
        'peso','altura', 'id_persona',
    ];
    public function persona(){
    	return $this->belongsTo('App\Persona');
    	//hasMany(Persona::class) para relaciones de 1 a M
    }
    public function cita(){
        return $this->hasMany('App\Cita');
        //hasMany(Persona::class) para relaciones de 1 a M
    }
}
