<?php

namespace App\Http\Controllers;
use App\Medico;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class MedicoController extends Controller
{
	use RegistersUsers;
    

	protected function validator(array $data)
    {
	   $persona= Persona::where("cedula",$data['cedula'])->get();
       if($persona->count()<0){
        return new errors("error");
       } 	
        return Validator::make($data, [
            'fecha_IngresoHospital' => ['required', 'string', 'max:255'],
            'cedula'=> ['required', 'string'],
        ]);
    }
    public function registro(){

    	return view('auth.registermedico'); 
    }
    public function showlogin(){

        return view('auth.loginmedico'); 
    }
    public function openHome(Request $request){
        //$persona=DB::table('personas')->where('cedula',$request->input('cedula'))->value("cedula");
        $persona=Persona::where('cedula',$request->input('cedula'))->first();
        $passwordEn= $request->input('password');
        $cedula=$persona['cedula'];
        if(($request->input('cedula')==$persona['cedula']) && password_verify($passwordEn,$persona['password'])){
            return view('homemedico')->with(compact('cedula'));
        }
        
        return back()->with('msj',"Error de credenciales"); 
    }
    public function show(){
        $medico=Medico::all();
        if(!is_null($medico)){
            $personasMed=array($medico->count());
            $Medicodato=array($medico->count());
            $countMedico=0;
            foreach ($medico as $key => $med) {
                $personasMed[$countMedico]=DB::table('personas')->where('id',$med['id_persona'])->select("name","apellido","cedula","telefono_Personal","email","direccion")->get();
                $Medicodato[$countMedico]=$med;
                $countMedico+=1;
            }
            return view('tablemedico')->with(compact('personasMed','Medicodato')); 
        }
        

        return view('tablepersona'); 
    }
    
    protected function create(array $data)
    {
       $persona=DB::table('personas')->where('cedula',$data['cedula'])->value('id');
       
       
        return Medico::create([
            'fecha_IngresoHospital' => $data['fecha_IngresoHospital'],
            'estado_Contrato' => $data['estado_Contrato'],
            'id_persona' => $persona,
            
        ]);
    }
}
