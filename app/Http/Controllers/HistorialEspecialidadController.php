<?php

namespace App\Http\Controllers;


use App\HistorialEspecialidad;
use App\Especialidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;


class HistorialEspecialidadController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'nivel' => ['required', 'string'],
            'casa_Estudio' => ['required', 'string'],
            'fecha_Inicio_Ejercion' => ['required', 'string'],
            'cedula' => ['required', 'string'],
            'id_especialidad' => ['required', 'string'],
        ]);
    }
    public function registro(){
        $especialidads=Especialidad::all();
    	return view('auth.registerhistorialespecialidad')->with(compact('especialidads'));
    }
    protected function create(array $data)
    {
        $mediCedula=DB::table('personas')->where('cedula',$data['cedula'])->value('id');
        $medico=DB::table('medicos')->where('id_persona',$mediCedula)->value('id');

        return HistorialEspecialidad::create([
            'nivel' => $data['nivel'],
            'casa_Estudio' => $data['casa_Estudio'],
            'fecha_Inicio_Ejercion' => $data['fecha_Inicio_Ejercion'],
            'id_medico' => $medico,
            'id_especialidad' => $data['id_especialidad'],
        ]);
    }
}
