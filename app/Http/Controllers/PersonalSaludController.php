<?php

namespace App\Http\Controllers;

use App\Persona;
use App\PersonalSalud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class PersonalSaludController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'cargo' => ['required', 'string'],
            'area_Trabajo' => ['required', 'string'],
            'fecha_ingreso_hospital' => ['required', 'string'],
            'estado_contrato' => ['required', 'string'],
            'cedula' => ['required', 'string'],
            
        ]);
    }
    public function show(){
        $ps=PersonalSalud::all();
        if(!is_null($ps)){
            $personasSalud=array($ps->count());
            $PSaluddato=array($ps->count());
            $countPsalud=0;
            foreach ($ps as $key => $psalud) {
                $personasSalud[$countPsalud]=DB::table('personas')->where('id',$psalud['id_persona'])->select("name","apellido","cedula","telefono_Personal","email","direccion")->get();
                $PSaluddato[$countPsalud]=$psalud;
                $countPsalud+=1;
            }
            return view('tablepersonalsalud')->with(compact('personasSalud','PSaluddato')); 
        }
        

        return view('tablepersonalsalud'); 
    }
    public function registro(){

    	return view('auth.registerpersonalsalud'); 
    }
    protected function create(array $data)
    {
        $personalsalud=DB::table('personas')->where('cedula',$data['cedula'])->value('id');
        return PersonalSalud::create([
            'cargo' => $data['cargo'],
            'area_Trabajo' => $data['area_Trabajo'],
            'fecha_ingreso_hospital' => $data['fecha_ingreso_hospital'],
            'estado_contrato' => $data['estado_contrato'],
            'id_persona' => $personalsalud,
        ]);
    }
}
