<?php

namespace App\Http\Controllers;

use App\HistorialMedicina;
use App\Medicinas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class HistorialMedicinaController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'fecha_prescripcion' => ['required', 'string'],
            'fecha_final_consumo' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'id_tratamiento' => ['required', 'string'],
        ]);
    }
    public function registro(){
        $medicina=Medicinas::all();
    	return view('auth.registerhistorialMedicina')->with(compact('medicina'));
    }
    protected function create(array $data)
    {
        return HistorialMedicina::create([
            'fecha_prescripcion' => $data['fecha_prescripcion'],
            'fecha_final_consumo' => $data['fecha_final_consumo'],
            'descripcion' => $data['descripcion'],
            'id_tratamiento' => $data['id_tratamiento'],
            'id_medicina' => $data['id_medicina'],
        ]);
    }
}
