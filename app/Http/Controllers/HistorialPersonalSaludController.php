<?php

namespace App\Http\Controllers;

use App\HistorialPersonalSalud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class HistorialPersonalSaludController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'descripcion' => ['required', 'string'],
            'cedula_personal_salud' => ['required', 'string'],
            'id_historial_procedimiento' => ['required', 'string'],
            
        ]);
    }
    public function registro(){

    	return view('auth.registerhistorialPersonalSalud'); 
    }
    protected function create(array $data)
    {
        $PSCedula=DB::table('personas')->where('cedula',$data['cedula_personal_salud'])->value('id');
        $PS=DB::table('personal_saluds')->where('id_persona',$PSCedula)->value('id');
        return HistorialPersonalSalud::create([
            'descripcion' => $data['descripcion'],
            'id_personal_saluds' => $PS,
            'id_historial_procedimientos' => $data['id_historial_procedimiento'],

        ]);
    }
}
