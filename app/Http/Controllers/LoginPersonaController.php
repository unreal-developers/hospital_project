<?php

namespace App\Http\Controllers;

use App\Medico;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Collection;

class LoginPersonaController extends Controller
{
    use AuthenticatesUsers;

    

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function credentials(Request $request)
 	{
		 $login = $request->input($this->username());

		// Comprobar si el input coincide con el formato de E-mail
		 $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'cedula' : 'email';

		return [
		 $field => $login,
		 'password' => $request->input('password')
	 	];
 	}

	 public function username()
	 {
	 	return 'login';
	 }
}
