<?php

namespace App\Http\Controllers;

use App\Diagnostico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class DiagnosticoController extends Controller
{
     use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre_clave' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'id_cita' => ['required', 'string'],
            
        ]);
    }
    public function registro(){

    	return view('auth.registerdiagnostico'); 
    }
    protected function create(array $data)
    {
        return Diagnostico::create([
            'nombre_clave' => $data['nombre_clave'],
            'descripcion' => $data['descripcion'],
            'id_cita' => $data['id_cita'],

        ]);
    }
}
