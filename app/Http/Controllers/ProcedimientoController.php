<?php

namespace App\Http\Controllers;

use App\Procedimiento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class ProcedimientoController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string'],
            'tipo_Procedimiento' => ['required', 'string'],
            'nivel_de_riesgo' => ['required', 'string'],
            'costo' => ['required', 'string'],
            'requisitos_estado_salud' => ['required', 'string'],
            'ambulatorio' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            
        ]);
    }
    public function show(){
        $procedimiento=Procedimiento::all();
        if(!is_null($procedimiento)){
           

            return view('tableprocedimiento')->with(compact('procedimiento')); 
        }
        

        return view('tableprocedimiento'); 
    }
    public function registro(){

    	return view('auth.registerprocedimiento'); 
    }
    protected function create(array $data)
    {
        return Procedimiento::create([
            'nombre' => $data['nombre'],
            'tipo_Procedimiento' => $data['tipo_Procedimiento'],
            'nivel_de_riesgo' => $data['nivel_de_riesgo'],
            'costo' => $data['costo'],
            'requisitos_estado_salud' => $data['requisitos_estado_salud'],
            'ambulatorio' => $data['ambulatorio'],
            'descripcion' => $data['descripcion'],
        ]);
    }
}
