<?php

namespace App\Http\Controllers;

use App\Medicinas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Collection;

class MedicinaController extends Controller
{
     use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string'],
            'nombre_cientifico' => ['required', 'string'],
            'gramos' => ['required', 'string'],
            'tipoEstado' => ['required', 'string'],
        ]);
    }
    public function show(){
        $medicinas=Medicinas::all();
        if(!is_null($medicinas)){
           

            return view('tablemedicinas')->with(compact('medicinas')); 
        }
        

        return view('tablemedicinas'); 
    }
    public function registro(){

    	return view('auth.registerMedicina'); 
    }
    protected function create(array $data)
    {
        return Medicinas::create([
            'nombre' => $data['nombre'],
            'nombre_cientifico' => $data['nombre_cientifico'],
            'gramos' => $data['gramos'],
            'tipoEstado' => $data['tipoEstado'],

        ]);
    }
}
