<?php

namespace App\Http\Controllers;

use App\Especialidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class EspecialidadController extends Controller
{
    use RegistersUsers;

	protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            
        ]);
    }
    public function registro(){

    	return view('auth.registerespecialidad'); 
    }
    public function show(){
        $especialidad=Especialidad::all();
        if(!is_null($especialidad)){
           

            return view('tableespecialidad')->with(compact('especialidad')); 
        }
        

        return view('tableespecialidad'); 
    }
    protected function create(array $data)
    {
        return Especialidad::create([
            'nombre' => $data['nombre'],
            'descripcion' => $data['descripcion'],
        ]);
    }
}
