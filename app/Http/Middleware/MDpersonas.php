<?php

namespace App\Http\Middleware;
use App\Persona;
use App\Medico;
use App\Paciente;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Closure;

class MDpersonas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        $cedula=\Auth::user();
        $pasCedula=DB::table('personas')->where('cedula',$cedula->cedula)->value('id');
        $mediCedula=DB::table('personas')->where('cedula',$cedula->cedula)->value('id');
        $paciente=DB::table('pacientes')->where('id_persona',$pasCedula)->value('id');
        $medico=DB::table('medicos')->where('id_persona',$mediCedula)->value('id');

        if(is_null($paciente)){
            return route("loginpaciente")->with("msj","Paciente no registrado");
        }else if(is_null($medico)){
            return route("loginmedico")->with("msj","Medico no registrado");
        }
        return $next($request);
    }
}
