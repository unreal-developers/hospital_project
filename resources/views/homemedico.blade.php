@extends('layouts.app')

@section('content')
<br/><br/><br/><br/><br/>
<div class="container">
    <div class="row justify-content-center">

        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Citas</h5><br/>
            <a href="{{ route('tableCitasMedico') }}" class="btn btn-block btn-primary text-center">Ver</a> 
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Diagnosticos Paciente</h5><br/>
            <a href="{{ route('registerpaciente') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Tratamientos Paciente</h5><br/>
            <a href="{{ route('registerpaciente') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <input type="hidden" id="cedula"  name="cedula" value="{{$cedula}}" /><br/>
        
    </div>
</div>
@endsection
