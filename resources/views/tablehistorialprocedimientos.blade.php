@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b110">
                <table data-vertable="ver1" id="table" data-toggle="table" data-url="/views/tablehistorialprocedimientos" data-show-export="true" data-pagination="true"
                data-resizable="true" data-click-to-select="true" data-toolbar="#toolbar"
       data-height="480" data-show-columns="true" data-search="true" data-flat="true"
       data-show-multi-sort="true"  data-sort-priority='[{"sortName": "Nombre","sortOrder":"desc"},{"sortName":"Focus","sortOrder":"desc"}]' data-filter-control="true" data-filter-show-clear="true">
                    <thead>
                        <tr class="row100 head">
                           
                            <th class="column100 column1" data-column="column1">Fecha Realizacion</th>
                            <th class="column100 column2" data-column="column2">Estado Realizacion</th>
                            <th class="column100 column3" data-column="column3">ID tratamiento</th>
                            <th class="column100 column4" data-column="column4">ID procedimiento</th>
                            <th class="column100 column5" data-column="column9">Editar</th>
                            <th class="column100 column6" data-column="column10">Eliminar</th>
                        </tr>

                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($hp as $k => $hpro)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-field="row".{{$k}} data-column="column1" id="column1">{{$hpro->fecha_realizacion}}</td>
                                <td class="column100 column2" data-column="column2">{{$hpro->estado_realizacion}}</td>
                                <td class="column100 column3" data-column="column3">{{$hpro->id_tratamiento}}</td>
                                <td class="column100 column4" data-column="column4">{{$hpro->id_procedimiento}}</td>
                                <td class="column100 column5" data-column="column9" onclick="obtenerValores()"><a href="#"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column6" data-column="column10"><a href="#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>

                        @endforeach


                        <div id="chart_div">Aqui</div>
                    </tbody>
                </table>
            </div>
        </div>

</div>
@endsection