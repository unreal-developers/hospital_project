@extends('layouts.app')
@section('content')
<div class="container" style="justify-content: center;">
<div class="limiter">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b-110">
                <table data-vertable="ver1">
                    <thead>
                        <tr class="row100 head">
                            <th class="column100 column1" data-column="column1">Nombre Medicamento</th>
                            <th class="column100 column2" data-column="column2">Nombre Cientifico</th>
                            <th class="column100 column3" data-column="column3">Gramos</th>
                            <th class="column100 column4" data-column="column4">Estado del Medicamento</th>
                            
                            <th class="column100 column5" data-column="column9">Editar</th>
                            <th class="column100 column6" data-column="column10">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($medicinas as $k => $med)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-column="column1">{{$med->nombre}}</td>
                                <td class="column100 column2" data-column="column2">{{$med->nombre_cientifico}}</td>
                                <td class="column100 column3" data-column="column3">{{$med->gramos}}</td>
                                <td class="column100 column4" data-column="column4">{{$med->tipoEstado}}</td>
                                <td class="column100 column5" data-column="column9"><a href="/#"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column6" data-column="column10"><a href="/#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>
                        @endforeach


                    
                    </tbody>
                </table>
            </div>
        </div>
     
    </div>

</div>
@endsection