@extends('layouts.app')

@section('content')
<br/><br/><br/><br/><br/>
<div class="container">
    <div class="row justify-content-center">

        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Datos Persona</h5>
            <a href="{{ route('registerpersona') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Paciente</h5><br/>
            <a href="{{ route('registerpaciente') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Medico</h5><br/>
            <a href="{{ route('registermedico') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Especialidad</h5><br/>
            <a href="{{ route('registerespecialidad') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Especialidad de Medico</h5>
            <a href="{{ route('registerhistorialespecialidad') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Personal de Salud</h5>
            <a href="{{ route('registerpersonalsalud') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Cita</h5><br/>
            <a href="{{ route('registercitas') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Diagnostico</h5><br/>
            <a href="{{ route('registerdiagnostico') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Tratamiento</h5><br/>
            <a href="{{ route('registertratamiento') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Procedimiento a un Tratamiento</h5>
            <a href="{{ route('registerhistorialProcedimiento') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Personal de salud en procedimientos</h5>
            <a href="{{ route('registerhistorialPersonalSalud') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Procedimiento</h5><br/>
            <a href="{{ route('registerprocedimiento') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Medicinas</h5><br/>
            <a href="{{ route('registerMedicina') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Medicina a un Tratamiento</h5>
            <a href="{{ route('registerhistorialMedicina') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Registrar Administrador</h5><br/>
            <a href="{{ route('registeradministrador') }}" class="btn btn-block btn-primary text-center">Registrar</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista  de Paciente</h5><br/>
            <a href="{{ route('tablepaciente') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Medico</h5><br/>
            <a href="{{ route('tablemedico') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Personal de Salud</h5>
            <a href="{{ route('tablepersonalsalud') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Cita</h5><br/>
            <a href="{{ route('tablecitas') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Procedimiento</h5><br/>
            <a href="{{ route('tableprocedimiento') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Medicinas</h5><br/>
            <a href="{{ route('tablemedicinas') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Especialidades</h5><br/>
            <a href="{{ route('tableespecialidad') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Lista de Historiales Procedimientos</h5><br/>
            <a href="{{ route('tablehistorialprocedimientos') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
      
    </div>
</div>
@endsection
