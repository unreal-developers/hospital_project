@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registertratamiento') }}">
                        @csrf
                        
                        <div class="form-group row">
                            <label for="descripcion_Medicamentos" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion Medicamentos') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion_Medicamentos" type="text" class="form-control{{ $errors->has('descripcion_Medicamentos') ? ' is-invalid' : '' }} z-depth-1" name="descripcion_Medicamentos" value="{{ old('descripcion_Medicamentos') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion_Medicamentos'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion_Medicamentos') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descripcion_Operaciones" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion Procedimientos') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion_Operaciones" type="text" class="form-control{{ $errors->has('descripcion_Operaciones') ? ' is-invalid' : '' }} z-depth-1" name="descripcion_Operaciones" value="{{ old('descripcion_Operaciones') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion_Operaciones'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion_Operaciones') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="id_diagnostico" class="col-md-4 col-form-label text-md-right">{{ __('Identificador Diagnostico') }}</label>

                            <div class="col-md-6">
                                <input id="id_diagnostico" type="text" class="form-control{{ $errors->has('id_diagnostico') ? ' is-invalid' : '' }}" name="id_diagnostico" value="{{ old('id_diagnostico') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('id_diagnostico'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_diagnostico') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection