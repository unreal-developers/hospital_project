@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerpersonalsalud') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="cargo" class="col-md-4 col-form-label text-md-right">{{ __('Cargo') }}</label>

                            <div class="col-md-6">
                                <input id="cargo" type="text" class="form-control{{ $errors->has('cargo') ? ' is-invalid' : '' }}" name="cargo" value="{{ old('cargo') }}" required autofocus>

                                @if ($errors->has('cargo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cargo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="area_Trabajo" class="col-md-4 col-form-label text-md-right">{{ __('Area de Trabajo') }}</label>

                            <div class="col-md-6">
                                <input id="area_Trabajo" type="text" class="form-control{{ $errors->has('area_Trabajo') ? ' is-invalid' : '' }}" name="area_Trabajo" value="{{ old('area_Trabajo') }}" required autofocus>

                                @if ($errors->has('area_Trabajo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('area_Trabajo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Inicio Contrato</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_ingreso_hospital" type="text" readonly  name="fecha_ingreso_hospital" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-sm-right">Estado Contrato</label>
                                    <div class="col-md-6"> 
                                        <select class="browser-default custom-select" name="estado_contrato" id="estado_contrato">
                                          <option selected>Escoge tu Estado Contrato</option>
                                          <option value="Vigente">Vigente</option>
                                          <option value="Vencido">Vencido</option>
                                        </select>
                                    </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="cedula" class="col-md-4 col-form-label text-md-right">{{ __('Cedula de Identidad del Personal de Salud') }}</label>

                            <div class="col-md-6">
                                <input id="cedula" type="text" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="cedula" value="{{ old('cedula') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('cedula'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cedula') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection