@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registermedico') }}">
                        @csrf

                         <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Inicio Contrato</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_IngresoHospital" type="text" readonly  name="fecha_IngresoHospital" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-sm-right">Estado Contrato</label>
                                    <div class="col-md-6"> 
                                        <select class="browser-default custom-select" name="estado_Contrato" id="estado_Contrato">
                                          <option selected>Escoge el Estado del Contrato</option>
                                          <option value="Vigente">Vigente</option>
                                          <option value="Vencido">Vencido</option>
                                        </select>
                                    </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="cedula" class="col-md-4 col-form-label text-md-right">{{ __('Cedula de Identidad del Medico') }}</label>

                            <div class="col-md-6">
                                <input id="cedula" type="text" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="cedula" value="{{ old('cedula') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('cedula'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cedula') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
