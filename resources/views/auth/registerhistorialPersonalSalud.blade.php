@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerhistorialPersonalSalud') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion" type="text" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} z-depth-1" name="descripcion" value="{{ old('descripcion') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cedula_personal_salud" class="col-md-4 col-form-label text-md-right">{{ __('Cedula del Personal Salud') }}</label>

                            <div class="col-md-6">
                                <input id="cedula_personal_salud" type="text" class="form-control{{ $errors->has('cedula_personal_salud') ? ' is-invalid' : '' }}" name="cedula_personal_salud" value="{{ old('cedula_personal_salud') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('cedula_personal_salud'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cedula_personal_salud') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_historial_procedimiento" class="col-md-4 col-form-label text-md-right">{{ __('Identificador de Historial de Procedimiento') }}</label>

                            <div class="col-md-6">
                                <input id="id_historial_procedimiento" type="text" class="form-control{{ $errors->has('id_historial_procedimiento') ? ' is-invalid' : '' }}" name="id_historial_procedimiento" value="{{ old('id_historial_procedimiento') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('id_historial_procedimiento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_historial_procedimiento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection