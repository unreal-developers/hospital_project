@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('registerprocedimiento') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerprocedimiento') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Procedimiento') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Tipo Procedimiento</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="tipo_Procedimiento" id="tipo_Procedimiento">
                                  <option selected>Escoge el Tipo de Procedimiento</option>
                                  <option value="Quirurgico">Cirugia</option>
                                  <option value="Examen">Examen</option>
                                  <option value="Examen">Terapia</option>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Nivel de Riesgo</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="nivel_de_riesgo" id="nivel_de_riesgo">
                                  <option selected>Escoge el Nivel de Riesgo</option>
                                  <option value="Bajo">Bajo</option>
                                  <option value="Medio">Medio</option>
                                  <option value="Alto">Alto</option>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="costo" class="col-md-4 col-form-label text-md-right">{{ __('Costo') }}</label>

                            <div class="col-md-6">
                                <input id="costo" type="text" class="form-control{{ $errors->has('costo') ? ' is-invalid' : '' }}" name="costo" value="{{ old('costo') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('costo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('costo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="requisitos_estado_salud" class="col-md-4 col-form-label text-md-right">{{ __('Requisitos Estado Salud') }}</label>

                            <div class="col-md-6">
                                <textarea  id="requisitos_estado_salud" type="text" class="form-control{{ $errors->has('requisitos_estado_salud') ? ' is-invalid' : '' }} z-depth-1" name="requisitos_estado_salud" value="{{ old('requisitos_estado_salud') }}" required autofocus></textarea>

                                @if ($errors->has('requisitos_estado_salud'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('requisitos_estado_salud') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">¿Es Ambolatoria?</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="ambulatorio" id="ambulatorio">
                                  <option selected>Escoge ¿Es Ambolatoria?</option>
                                  <option value="si">Si</option>
                                  <option value="no">No</option>
                                </select>
                            </div>                            
                        </div>
                        
                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion" type="text" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} z-depth-1" name="descripcion" value="{{ old('descripcion') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection