@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="card" style="width: 18rem;">
      <img class="card-img-top" data-src="holder.js/100px180/" alt="100%x180" style="height: 200px; width: 100%; display: block;" src="{{ asset('img/admi.jpg') }}" data-holder-rendered="true">
      <div class="card-body">
        <h3 class="card-title">Administrador</h3>
        <a href="{{route('login')}}" class="btn btn-primary">Login</a>
      </div>
    </div> 
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <div class="row justify-content-center">
      <div class="card" style="width: 18rem;">
      <img class="card-img-top" data-src="holder.js/100px180/" alt="100%x180" style="height: 200px; width: 100%; display: block;" src="{{ asset('img/medico.jpg') }}" data-holder-rendered="true">
      <div class="card-body">
        <h3 class="card-title">Medico</h3>
        <a href="{{ route('loginmedico') }}" class="btn btn-primary">Login</a>
      </div>
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <div class="row justify-content-center">
      <div class="card" style="width: 18rem;">
      <img class="card-img-top" data-src="holder.js/100px180/" alt="100%x180" style="height: 200px; width: 100%; display: block;" src="{{ asset('img/paciente.jpg') }}" data-holder-rendered="true">
      <div class="card-body">
        <h3 class="card-title">Paciente</h3>
        <a href="{{ route('loginpaciente') }}" class="btn btn-primary">Login</a>
      </div>
    </div>
    </div>

</div>
@endsection
