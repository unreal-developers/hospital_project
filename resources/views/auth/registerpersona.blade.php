@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4 class="text-uppercase text-black font-weight-bold">REGISTRO</h4></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerpersona') }}">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="apellido" class="col-md-4 col-form-label text-md-right">{{ __('Apellido') }}</label>

                                    <div class="col-md-6">
                                        <input id="apellido" type="text" class="form-control{{ $errors->has('apellido') ? ' is-invalid' : '' }}" name="apellido" value="{{ old('apellido') }}" required autofocus>

                                        @if ($errors->has('apellido'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('apellido') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="cedula" class="col-md-4 col-form-label text-md-right">{{ __('Cedula de Identidad') }}</label>

                                    <div class="col-md-6">
                                        <input id="cedula" type="text" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="cedula" value="{{ old('cedula') }}" onkeypress="validate(event)" required>

                                        @if ($errors->has('cedula'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('cedula') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                 <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-md-right">Fecha Nacimiento</label>
                                    <div class="col-md-6"> 
                                        <div class="col-xs-3">
                                            <div class="controls">
                                                <input class="datepicker form-control" id="fecha_nacimiento" type="text" readonly  name="fecha_nacimiento" data-date-format="yyyy/ mm/dd"/>
                                                
                                            </div>

                                    </div>         
                                    </div>                            
                                </div>
                                <div class="form-group row">
                                    <label for="edad" class="col-md-4 col-form-label text-md-right">{{ __('Edad') }}</label>

                                    <div class="col-md-6">
                                        <input id="edad" type="text" class="form-control{{ $errors->has('edad') ? ' is-invalid' : '' }}" name="edad" value="{{ old('edad') }}" onkeypress="validate(event)" required>


                                        @if ($errors->has('edad'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('edad') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-sm-right">Sexo</label>
                                    <div class="col-md-6"> 
                                        <select class="browser-default custom-select" name="sexo" id="sexo">
                                          <option selected>Escoge tu sexo</option>
                                          <option value="masculino">Masculino</option>
                                          <option value="femenino">Femenino</option>
                                        </select>
                                    </div>                            
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label text-sm-right">Tipo de Sangre</label>
                                    <div class="col-md-6"> 
                                        <select class="browser-default custom-select" name="tipo_Sangre" id="tipo_Sangre">
                                          <option selected>Escoge tu tipo de sangre</option>
                                          <option value="O-">O-</option>
                                          <option value="O+">O+</option>
                                          <option value="A-">A-</option>
                                          <option value="A+">A+</option>
                                          <option value="B-">B-</option>
                                          <option value="B+">B+</option>
                                          <option value="AB-">AB-</option>
                                          <option value="AB+">AB+</option>
                                        </select>
                                    </div>                            
                                </div>
                               
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group row">
                                    <label for="telefono_Personal" class="col-md-4 col-form-label text-md-right">{{ __('Telefono Personal') }}</label>

                                    <div class="col-md-6">
                                        <input id="telefono_Personal" type="text" class="form-control{{ $errors->has('telefono_Personal') ? ' is-invalid' : '' }}" name="telefono_Personal" value="{{ old('telefono_Personal') }}" onkeypress="validate(event)" required>


                                        @if ($errors->has('telefono_Personal'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('telefono_Personal') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="telefono_Emergencia" class="col-md-4 col-form-label text-md-right">{{ __('Telefono Emergencia') }}</label>

                                    <div class="col-md-6">
                                        <input id="telefono_Emergencia" type="text" class="form-control{{ $errors->has('telefono_Emergencia') ? ' is-invalid' : '' }}" name="telefono_Emergencia" value="{{ old('telefono_Emergencia') }}" onkeypress="validate(event)" required>

                                        @if ($errors->has('telefono_Emergencia'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('telefono_Emergencia') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo Electronico') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                                    <div class="col-md-6">
                                        <input id="direccion" type="text" class="form-control{{ $errors->has('direccion') ? ' is-invalid' : '' }}" name="direccion" value="{{ old('direccion') }}" required autofocus>

                                        @if ($errors->has('direccion'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('direccion') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
