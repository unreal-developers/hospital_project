@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4 class="text-uppercase text-black font-weight-bold">REGISTRO</h4></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerhistorialespecialidad') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Nivel de Especialidad</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="nivel" id="nivel">
                                  <option selected>Escoge el nivel de especialidad</option>
                                  <option value="Licenciatura">Licenciatura</option>
                                  <option value="Magister">Magister</option>
                                  <option value="Doctorado">Doctorado</option>
                                </select>
                            </div>                            
                        </div>

                        <div class="form-group row">
                            <label for="casa_Estudio" class="col-md-4 col-form-label text-md-right">{{ __('Nombre de Casa de Estudio') }}</label>

                            <div class="col-md-6">
                                <input id="casa_Estudio" type="text" class="form-control{{ $errors->has('casa_Estudio') ? ' is-invalid' : '' }}" name="casa_Estudio" value="{{ old('casa_Estudio') }}" required autofocus>

                                @if ($errors->has('casa_Estudio'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('casa_Estudio') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Fecha Inicio de Laborable') }}</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_Inicio_Ejercion" type="text" readonly  name="fecha_Inicio_Ejercion" data-date-format="yyyy/ mm/dd"/>
                                        
                                    </div>

                            </div>         
                            </div>                            
                        </div>

                        <div class="form-group row">
                            <label for="cedula" class="col-md-4 col-form-label text-md-right">{{ __('Cedula de Identidad del Medico') }}</label>

                            <div class="col-md-6">
                                <input id="cedula" type="text" class="form-control{{ $errors->has('cedula') ? ' is-invalid' : '' }}" name="cedula" value="{{ old('cedula') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('cedula'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cedula') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Especialidad</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="id_especialidad" id="id_especialidad">
                                  <option selected>Escoge la Especialidad</option>
                                  @foreach ($especialidads as $especialidad)
                                    <option value={{$especialidad->id}}>{{$especialidad->nombre}}</option>
                                  @endforeach
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                                
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection