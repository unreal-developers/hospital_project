@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registerhistorialMedicina') }}">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Prescripcion</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_prescripcion" type="text" readonly  name="fecha_prescripcion" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fecha Final Consumo</label>
                            <div class="col-md-6"> 
                                <div class="col-xs-3">
                                    <div class="controls">
                                        <input class="datepicker form-control" id="fecha_final_consumo" type="text" readonly  name="fecha_final_consumo" data-date-format="yyyy/mm/dd"/>
                                        
                                    </div>

                            </div>         
                        </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>

                            <div class="col-md-6">
                                <textarea  id="descripcion" type="text" class="form-control{{ $errors->has('descripcion') ? ' is-invalid' : '' }} z-depth-1" name="descripcion" value="{{ old('descripcion') }}" required autofocus></textarea>

                                @if ($errors->has('descripcion'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('descripcion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-sm-right">Medicina del tratamiento</label>
                            <div class="col-md-6"> 
                                <select class="browser-default custom-select" name="id_medicina" id="id_medicina">
                                  <option selected>Seleccion la Medicina</option>
                                  @foreach ($medicina as $med)
                                    <option value={{$med->id}}>{{$med->nombre}} - {{$med->nombre_cientifico}}</option>
                                  @endforeach  
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group row">
                            <label for="id_tratamiento" class="col-md-4 col-form-label text-md-right">{{ __('Identificador de Tratamiento') }}</label>

                            <div class="col-md-6">
                                <input id="id_tratamiento" type="text" class="form-control{{ $errors->has('id_tratamiento') ? ' is-invalid' : '' }}" name="id_tratamiento" value="{{ old('id_tratamiento') }}" onkeypress="validate(event)" required>

                                @if ($errors->has('id_tratamiento'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_tratamiento') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection