@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b110">
                <table data-vertable="ver1">
                    <thead>
                        <tr class="row100 head">
                            <th class="column100 column1" data-column="column1">Nombre</th>
                            <th class="column100 column2" data-column="column2">Descripcion</th>
                            <th class="column100 column3" data-column="column9">Editar</th>
                            <th class="column100 column4" data-column="column10">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($especialidad as $k => $es)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-column="column1">{{$es->nombre}}</td>
                                <td class="column100 column2" data-column="column2">{{$es->descripcion}}</td>
                                <td class="column100 column9" data-column="column3"><a href="/#"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column10" data-column="column4"><a href="/#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>
                        @endforeach


                    
                    </tbody>
                </table>
            </div>
        </div>

</div>
@endsection