@extends('layouts.app')

@section('content')
<br/><br/><br/><br/><br/>
<div class="container">
    <div class="row justify-content-center">

        
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Historial Tratamiento</h5><br/>
            <a href="{{ route('registermedico') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
        
        <div class="card text-white bg-secondary mb-3" style="width: 13rem; height: 8rem;">
          <div class="card-body">
            <h5 class="card-title">Historia Diagnosticos</h5><br/>
            <a href="{{ route('tablepaciente') }}" class="btn btn-block btn-primary text-center">Ver</a>
          </div>
        </div>
      
    </div>
</div>
@endsection
