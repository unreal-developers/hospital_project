@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b110">
                <table data-vertable="ver1" id="table" data-filter-show-clear="true">
                    <thead>
                        <tr class="row100 head">
                           
                            <th class="column100 column1" data-column="column1">Fecha Cita</th>
                            <th class="column100 column2" data-column="column2">Estado Consulta</th>
                            <th class="column100 column3" data-column="column3">Fecha Consulta</th>
                            <th class="column100 column4" data-column="column4">Hora Consulta</th>
                            <th class="column100 column5" data-column="column4">Estado Cita</th>
                            <th class="column100 column6" data-column="column4">ID paciente</th>
                            <th class="column100 column5" data-column="column9">Diagnostico</th>
                        </tr>

                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($citas as $k => $cita)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-field="row".{{$k}} data-column="column1" id="column1">{{$cita->fecha_cita}}</td>
                                <td class="column100 column2" data-column="column2">{{$cita->fecha_consulta}}</td>
                                <td class="column100 column3" data-column="column3">{{$cita->hora_consulta}}</td>
                                <td class="column100 column4" data-column="column4">{{$cita->estado_cita}}</td>
                                <td class="column100 column5" data-column="column4">{{$cita->id_paciente}}</td>
                                <td class="column100 column6" data-column="column9"><a href="#"><button class="btn btn-secondary text-white">Diagnostico </button></a></td>
                            </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>

</div>
@endsection