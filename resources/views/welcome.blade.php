<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hospital Control</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
         <!-- Font Awesome Icons -->
          <link href="{{ asset('WelcomePage/fontawesome-free/css/all.min.css') }} " rel="stylesheet" type="text/css">

          <!-- Google Fonts -->
          <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
          <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

          <!-- Plugin CSS -->
          <link href="{{ asset('WelcomePage/magnific-popup/magnific-popup.css') }} " rel="stylesheet">

          <!-- Theme CSS - Includes Bootstrap -->
          <link href="{{ asset('WelcomePage/css/creative.min.css') }} " rel="stylesheet">

    </head>
    <body id="page-top">
           <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Hospital Control</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto my-2 my-lg-0">
             @if (Route::has('login'))
                
                    @auth
                        <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/home') }}">Home</a>
                      </li>
                    @else
                        <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ route('selectlogin') }}">Login</a>
                      </li>
                       <!-- @if (Route::has('register'))
                            <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
                          </li>
                        @endif-->
                    @endauth
                
            @endif
          
        </ul>
      </div>
    </div>
  </nav>

  <!-- Masthead -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-center text-center">
        <div class="col-lg-10 align-self-end">
          <h1 class="text-uppercase text-white font-weight-bold">SISTEMA DE CONTROL DEL HOSPITAL</h1>
          <h2 class="text-uppercase text-white font-weight-bold">"LA VIDA ES BELLA"</h2>
          <hr class="divider my-4">
        </div>
        <div class="col-lg-8 align-self-baseline">
          <p class="text-white-75 font-weight-light mb-5">Sistema para el manejo de personal y citas del hospital "La vida es bella"</p>
        </div>
      </div>
    </div>
  </header>

  <!-- Services Section -->
  <section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">Nuestros Hospital</h2>
      <hr class="divider my-4">
      <div class="row">
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <img src=" {{ asset('WelcomePage/img/Hospital1.jpg') }} " alt="Rounded Image" class="rounded img-fluid">
            <h3 class="h4 mb-2">Instalaciones</h3>
            <p class="text-muted mb-0">Instalaciones innovadoras</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
           <img src=" {{ asset('WelcomePage/img/enfermeras.jpg') }} " alt="Rounded Image" class="rounded img-fluid">
            <h3 class="h4 mb-2">Enfermeras</h3>
            <p class="text-muted mb-0">Las mejores enfermeras preparadas para atenderles</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <img src=" {{ asset('WelcomePage/img/equipos.jpg') }} " alt="Rounded Image" class="rounded img-fluid">
            <h3 class="h4 mb-2">Equipos</h3>
            <p class="text-muted mb-0">Equipos de alta gama y funcionales</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 text-center">
          <div class="mt-5">
            <img src=" {{ asset('WelcomePage/img/Doctores.jpg') }} " alt="Rounded Image" class="rounded img-fluid">
            <h3 class="h4 mb-2">Medicos</h3>
            <p class="text-muted mb-0">Medicos certificados preparados para darle solucion a sus problemas</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Contact Section -->
  <section class="page-section" id="contact">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="mt-0">CONTACTANOS</h2>
          <hr class="divider my-4">
          <p class="text-muted mb-5">Comunicate con nosotros haz tus citas</p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 ml-auto text-center">
          <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
          <div>04249310446</div>
        </div>
        <div class="col-lg-4 mr-auto text-center">
          <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
          <!-- Make sure to change the email address in anchor text AND the link below! -->
          <div>hospitallavidaesbella@gmail.com</div>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">LD Copyright 2019</div>
    </div>
  </footer>

        <!-- Bootstrap core JavaScript -->
        <script src="{{ asset('WelcomePage/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }} "></script>

        <!-- Plugin JavaScript -->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <script src="{{ asset('WelcomePage/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
        <!-- Custom scripts for this template -->
        <script src="{{ asset('WelcomePage/js/creative.min.js') }}"></script>

    </body>
</html>
