<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hospital Control</title>

    <!-- Scripts -->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

 <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
        <link href="{{ asset('bootstrap/css/bootstrap-grid.min.css') }}" rel="stylesheet" id="bootstrap-css">
        <link href="{{ asset('bootstrap/css/bootstrap-reboot.min.css') }}" rel="stylesheet" id="bootstrap-css">


        <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
        
         <!-- Font Awesome Icons -->
          <link href="{{ asset('WelcomePage/fontawesome-free/css/all.min.css') }} " rel="stylesheet" type="text/css">

          <!-- Google Fonts -->
          <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
          <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

          <!-- Plugin CSS -->
          <link href="{{ asset('WelcomePage/magnific-popup/magnific-popup.css') }} " rel="stylesheet">

          <!-- Theme CSS - Includes Bootstrap -->
          <link href="{{ asset('WelcomePage/css/creative.min.css') }} " rel="stylesheet">
          <link href="{{ asset('WelcomePage/css/creative.min.css') }} " rel="stylesheet">
          <link href="{{ asset('WelcomePage/css/creative.min.css') }} " rel="stylesheet">
          <link href="{{ asset('DatePicker2/css/bootstrap-datetimepicker.css') }} " rel="stylesheet">
          
          <link href="{{ asset('select2/select2.min.css') }} " rel="stylesheet">
          <link href="{{ asset('css/main.css') }}" rel="stylesheet">
          <script src="{{ asset('jquery-3.4.0.min.js') }}"></script>
          <script src="{{ asset('DatePicker/js/bootstrap-datepicker.js') }}"></script>
          <script src="{{ asset('DatePicker2/js/bootstrap-datetimepicker.js') }}"></script>
          <script src="{{ asset('DatePicker2/js/locales/bootstrap-datetimepicker.es.js') }}"></script>

          <!--ClockPicker-->
          <link href="{{ asset('TimePicker/dist/bootstrap-clockpicker.min.css') }} " rel="stylesheet">
          <script src="{{ asset('TimePicker/assets/js/highlight.min.js') }}"></script>
          <script src="{{ asset('TimePicker/dist/bootstrap-clockpicker.min.js') }}"></script>

          <!--Tablas-->
          <link href="{{ asset('Tablas/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }} " rel="stylesheet">
          <link href="{{ asset('Tablas/vendor/animate/animate.css') }} " rel="stylesheet">
          <link href="{{ asset('Tablas/vendor/animate/animate.css') }} " rel="stylesheet">
          <link href="{{ asset('Tablas/vendor/select2/select2.min.css') }} " rel="stylesheet">
          <link href="{{ asset('Tablas/css/util.css') }} " rel="stylesheet">
          <link href="{{ asset('Tablas/css/main.css') }} " rel="stylesheet">
          


    </head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="/">Hospital Control</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                @guest
                    <li class="nav-item">
                        <!--<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        -->
                    </li>
                   <!-- @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Registrar') }}</a>
                        </li>
                    @endif-->
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('CERRAR SESIÓN') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
          </div>
        </div>
      </nav>
          <!-- Masthead -->
      <header class="masthead">
        <div class="container h-100">
          <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-lg-12 align-self">
              <main class="py-4">
                   @yield('content')
              </main>
              
          </div>
            </div>
        </div>
      </header>
      <section class="page-section bg-primary" id="about">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-10 text-center">
            <h1 class="text-uppercase text-white font-weight-bold">Sistema de Control del Hospital</h1>
            <h2 class="text-uppercase text-white font-weight-bold">"La vida es bella"</h2>
          </div>
        </div>
      </div>
    </section>  
    </div>
    <!-- Bootstrap core JavaScript -->
        
        <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }} "></script>
        <script src="{{ asset('tabledata.js') }} "></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js') }} "></script>
        <!-- Plugin JavaScript -->
        <script src="{{ asset('WelcomePage/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
        
        <script href="{{ asset('select2/select2.min.js') }} " rel="stylesheet"></script>
        <script href="{{ asset('js/global.js') }} " rel="stylesheet"></script>
        <script src="{{ asset('js/validador.js') }} "></script>

        <!-- Custom scripts for this template -->
        <script src="{{ asset('WelcomePage/js/creative.min.js') }}"></script>

        <!--Tablas Script-->
        <script src="{{ asset('Tablas/vendor/bootstrap/js/popper.js') }}"></script>
        <script src="{{ asset('Tablas/vendor/select2/select2.min.js') }}"></script>
        <script src="{{ asset('Tablas/js/main.js') }}"></script>
        
</body>
</html>
