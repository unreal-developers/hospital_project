@extends('layouts.app')
@section('route', "{{ route('registermedico') }}") 
@section('content')
<div class="container">
<div class="limiter">
        <div class="wrap-table100">
            <div class="table100 ver1 m-b-110">
                <table data-vertable="ver1">
                    <thead>
                        <tr class="row100 head">
                            <th class="column100 column1" data-column="column1">Nombre</th>
                            <th class="column100 column2" data-column="column2">Apellido</th>
                            <th class="column100 column3" data-column="column3">Cedula</th>
                            <th class="column100 column4" data-column="column4">Telefono Personal</th>
                            <th class="column100 column5" data-column="column5">Email</th>
                            <th class="column100 column6" data-column="column6">Direccion</th>
                            <th class="column100 column7" data-column="column7">Peso</th>
                            <th class="column100 column8" data-column="column8">Altura</th>
                            <th class="column100 column9" data-column="column9">Editar</th>
                            <th class="column100 column10" data-column="column10">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                                     
                        @foreach ($personasPas as $k => $pas)

                            <tr class="row100" id="row".{{$k}}>
                                <td class="column100 column1" data-column="column1">{{$pas[0]->name}}</td>
                                <td class="column100 column2" data-column="column2">{{$pas[0]->apellido}}</td>
                                <td class="column100 column3" data-column="column3">{{$pas[0]->cedula}}</td>
                                <td class="column100 column4" data-column="column4">{{$pas[0]->telefono_Personal}}</td>
                                <td class="column100 column5" data-column="column5">{{$pas[0]->email}}</td>
                                <td class="column100 column6" data-column="column6">{{$pas[0]->direccion}}</td>
                                <td class="column100 column7" data-column="column7">{{$pacientedato[0]->peso}}</td>
                                <td class="column100 column8" data-column="column8">{{$pacientedato[0]->altura}}</td>
                                <td class="column100 column9" data-column="column9"><a href="/edit/{{$pas[0]->cedula}}"><button class="btn btn-secondary text-white">Editar</button></a></td>
                                <td class="column100 column10" data-column="column10"><a href="/#"><button class="btn btn-secondary text-white">Eliminar</button></a></td>
                            </tr>
                        @endforeach


                    
                    </tbody>
                </table>
            </div>
        </div>
     
    </div>

</div>
@endsection